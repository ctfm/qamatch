﻿export class Question {
    id: number;
    surveyId: number;
    order: number;
    kind: string;
    title: string;
    description: string;
    choices: string[] | null;
    scale: number | null;
    scaleRange: [number, number] | null;
    weight: number | null;
    weightRange: [number, number] | null;

    public static ToString(question: Question): string {
        if (question != null) {
            return "id: " + question.id
                + " | surveyId: " + question.surveyId
                + " | title: " + question.title
                + " | weight: " + question.weight
                + " | scale: " + question.scale
                ;
        }
        return null;
    }
}