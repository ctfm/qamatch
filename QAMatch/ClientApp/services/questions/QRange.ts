﻿export class QRange {
    firstIndex: number;
    lastIndex: number;
    total: number;

    public static toString(range: QRange): string {
        if (range != null) {
            return "first: " + range.firstIndex
                + " | last: " + range.lastIndex
                + " | total: " + range.total
                ;
        }
        return null;
    }
}