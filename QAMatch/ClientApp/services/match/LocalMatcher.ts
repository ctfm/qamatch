﻿import { IMatcher } from "./IMatcher";
import { MatchSource } from "./MatchSource";
import { MatchResult } from "./MatchResult";
import { Answer } from "../answers/Answer";
import { DeltaTuple } from "./DeltaTuple";
import { AnswerTuple } from "./AnswerTuple";
import { DistanceValues } from "./DistanceValues";

export class LocalMatcher implements IMatcher {

    public getMatchAsync(source1: MatchSource, source2: MatchSource): Promise<MatchResult> {
        const result = this.getMatch(source1, source2);
        return new Promise<MatchResult>((resolve, reject) => resolve(result));
    }

    public getMatchesAsync(source1: MatchSource, otherSources: MatchSource[]): Promise<MatchResult[]> {
        const results: MatchResult[] = otherSources
            .map(source2 => this.getMatch(source1, source2));
        return new Promise<MatchResult[]>((resolve, reject) => resolve(results));
    }

    private getMatch(source1: MatchSource, source2: MatchSource): MatchResult {
        const deltas: AnswerTuple[] = this.getDeltas(source1, source2);
        const deltaStats: DistanceValues = this.getDeltaStats(deltas);
        const result: MatchResult = {
            sid: source1.surveyId || source2.surveyId, //check
            uid1: source1.userId,
            uid2: source2.userId,
            source1: source1,   //check: null?
            source2: source2,   //check: null?
            deltaTups: deltas,
            statistics: deltaStats
        };
        return result;
    }

    private getDeltas(source1: MatchSource, source2: MatchSource): AnswerTuple[] {
        const deltas: AnswerTuple[] = source1.answers
            .map(answer1 => {
                return {
                    qid: answer1.questionId,
                    aid1: answer1.id,
                    aid2: source2.answers
                        .filter(a => a.questionId == answer1.questionId)
                        .map(a => a.id)[0],
                    weight: answer1.weight,
                    range: answer1.scaleRange,
                    scale1: answer1.scale,
                    scale2: source2.answers
                        .filter(a => a.questionId == answer1.questionId)
                        .map(a => a.scale)[0]
                };
            })
            .map(scaleTup => {
                const deltaTup = this.getDistance(scaleTup.scale1, scaleTup.scale2, scaleTup.weight);
                const answerTup: AnswerTuple = {
                    qid: scaleTup.qid,
                    aid1: scaleTup.aid1,
                    scale1: scaleTup.scale1,
                    aid2: scaleTup.aid2,
                    scale2: scaleTup.scale2,
                    delta: deltaTup.delta,
                    weight: deltaTup.weight,
                    range: scaleTup.range
                };
                return answerTup;
            });
        return deltas;
    }

    private getDeltaStats(answerTups: AnswerTuple[]): DistanceValues {
        const maxDelta: number = this.getMaxDelta(answerTups[0], 4);
        const totalWeight: number = answerTups
            .reduce((acc, tup) => acc + tup.weight, 0);
        const totalDelta = answerTups
            .map(tup => tup.delta * tup.weight)
            .reduce((acc, delta) => acc + delta, 0);
        const averageDelta = totalDelta / totalWeight;
        const scorePercent: number = 100 - (averageDelta * 100 / maxDelta);
        const deltaStats: DistanceValues = {
            averageDistance: averageDelta,
            maxDistance: maxDelta,
            totalDistance: totalDelta,
            totalWeight: totalWeight,
            scorePercent: scorePercent
        };
        return deltaStats;
    }

    private getDistance(scale1: any, scale2: any, weight: number): DeltaTuple {
        const canMatch = !isNaN(scale1) && !isNaN(scale2);
        const delta: number = (canMatch) ? Math.abs(scale1 - scale2) : 0;
        const newWeight: number = (canMatch) ? weight : 0;
        return { delta: delta, weight: newWeight };
    }

    private getMaxDelta(answerTup: AnswerTuple, defDelta: number): number {
        const minRange: number = answerTup && answerTup.range[0];
        const maxRange: number = answerTup && answerTup.range[1];
        const maxDelta: number = (minRange && maxRange)
            ? maxRange - minRange
            : defDelta;
        return maxDelta;
    }

}