﻿export class AnswerHelper {

    public static getQuestionUrl(currentUrl: string, qix: number, offset: number): string {
        const questionPos = currentUrl.indexOf('question');
        const baseUrl: string = currentUrl.substring(0, questionPos);
        const nextQix = qix + offset;
        const newUrl: string = baseUrl + "question/" + nextQix + "/answer";
        console.log("currentUrl:   " + currentUrl);
        console.log("baseUrl:      " + baseUrl);
        console.log("newUrl:       " + newUrl);
        return newUrl;
    }

    public static getScaleColor(val: number, active: number): string {
        const isActive: boolean = (val == active);
        switch (val) {
            case -2: return (isActive) ? "on-red" : "off-red";
            case -1: return (isActive) ? "on-lightred" : "off-lightred";
            case 0: return (isActive) ? "on-white" : "off-white";
            case 1: return (isActive) ? "on-lightgreen" : "off-lightgreen";
            case 2: return (isActive) ? "on-green" : "off-green";
        }
    }

    public static getScaleButtonStyle(val: number, active: number): string {
        const color = this.getScaleColor(val, active);
        const style = "btn " + color + " size-20"
        return style;
    }

    public static getWeightColor(val: number, active: number): string {
        const isActive: boolean = (val == active);
        const color: string = (isActive) ? ("on-" + val) : ("off-" + val);
        return color;
    }

    public static getWeightButtonStyle(val: number, active: number): string {
        const color = this.getWeightColor(val, active);
        const style = "btn " + color + " size-20";
        return style;
    }

    public static getWeightTDColor(val: number): string {
        const color: string = "td-weight-" + val;
        return color;
    }

    public static getDeltaTDColor(val: number, weight: number): string {
        if (weight == 0) return "td-weight-0";
        switch (val) {
            case 4: return "td-delta-red";
            case 3: return "td-delta-lightred";
            case 2: return "td-delta-white";
            case 1: return "td-delta-lightgreen";
            case 0: return "td-delta-green";
        }
    }
}