﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import 'isomorphic-fetch';
import { Answer } from '../../services/answers/Answer';
import { DOMElement } from 'react';
import { SurveyUser } from './SurveyUser';
import { IAnswerRepos } from '../../services/answers/IAnswerRepos';
import { Question } from '../../services/questions/Question';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import { IMatcher } from '../../services/match/IMatcher';
import { Answers } from '../answers/Answers';
import { MatchSource } from '../../services/match/MatchSource';
import { ILogger } from '../../helper/logging/ILogger';
import { MatchHelper } from '../../helper/match/MatchHelper';
import { SidUidTuple } from '../../services/match/SidUidTuple';
import { IMatchRepos } from '../../services/match/IMatchRepos';
import { MatchResult } from '../../services/match/MatchResult';
import { CollectionHelper } from '../../helper/collection/CollectionHelper';
import { UrlHelper } from '../../helper/url/UrlHelper';
import { User } from '../../services/users/User';
import { IUserRepos } from '../../services/users/IUserRepos';

interface MatchProps {
    routeProps: RouteComponentProps<{}>;
    logger: ILogger;
    localAnswerRepos: IAnswerRepos;
    answerRepos: IAnswerRepos;
    questionRepos: IQuestionRepos;
    userRepos: IUserRepos;
    matcher: IMatcher;
    matchRepos: IMatchRepos;
}

interface MatchState {
    loading: boolean;
    answerSets: SurveyUser[];
    selectedSets: SurveyUser[];
    selection: number[];
    selectAll: boolean;
    users: User[];
}

export class MatchSources extends React.Component<MatchProps, MatchState> {
    constructor(props: MatchProps) {
        super(props);
        this.state = {
            loading: true,
            answerSets: [],
            selectedSets: [],
            selection: [],
            selectAll: false,
            users: []
        };
        this.handleToggleItem = this.handleToggleItem.bind(this);
        this.handleToggleAll = this.handleToggleAll.bind(this);
        this.handleMatch = this.handleMatch.bind(this);
    }

    public componentDidMount() {
        //const uid: number = this.props.routeProps.match.params["uid"];
        const sid: number = this.props.routeProps.match.params["sid"];
        this.fetchAndSetQandas(sid);
    }

    private fetchAndSetQandas(sid: number) {
        const answersPromise: Promise<number[]> = this.props.answerRepos.getUidsBySidAsync(sid);
        const userPromise: Promise<User[]> = this.props.userRepos.getUsersAsync();
        Promise.all([answersPromise, userPromise])
            .then(data => this.mergeData(sid, data[0], data[1]))
            .then(mData => this.setState((prev, props) => {
                return {
                    loading: false,
                    answerSets: mData[0],
                    selectedSets: mData[0],  //TODO: set by form handler!
                    users: mData[1]
                };
            }))
            .catch(reason => this.props.logger.error("no uids reason: " + reason));
    }

    private mergeData(sid: number, uids: number[], users: User[]): [SurveyUser[],User[]] {
        if (sid && uids) {
            const suIds: SurveyUser[] = uids
                .map(uid => { return { surveyId: sid, userId: uid }; });
            return [suIds, users];
        }
        return [null, null];
    }

    private handleToggleItem(event, uid: number) {
        this.setState((prev, props) => {
            return {
                selection: CollectionHelper.toggleItem(prev.selection, uid)
            }
        });
        this.props.logger.debug("MatchSources.handleToggleItem uid:           " + uid);
        this.props.logger.debug("MatchSources.handleToggleItem old selectAll: " + this.state.selectAll);
        this.props.logger.debug("MatchSources.handleToggleItem old selection: " + this.state.selection);
    }

    private handleToggleAll(event) {
        const newSelectAll = (this.state.selectAll) ? false : true;
        const newSelection = (newSelectAll)
            ? this.state.answerSets.map(suid => suid.userId)
            : [];
        this.setState((prev, props) => {
            return {
                selectAll: newSelectAll,
                selection: newSelection
            };
        });
        this.props.logger.debug("MatchSources.handleToggleAll old selectAll: " + this.state.selectAll);
        this.props.logger.debug("MatchSources.handleToggleAll new selectAll: " + newSelectAll);
        this.props.logger.debug("MatchSources.handleToggleAll old selection: " + this.state.selection);
        this.props.logger.debug("MatchSources.handleToggleAll new selection: " + newSelection);
    }

    private handleMatch(event, sid: number) {
        const logger = this.props.logger;
        const selectedUsers: SidUidTuple[] = this.state.answerSets
            .filter(suid => this.state.selection.some(uid => uid == suid.userId));
        const resultsUrl = UrlHelper.getMatchResultsUrl(sid);
        logger.debug("MatchSources.handleMatch answerSets:\n"     + SidUidTuple.ToStrings(this.state.answerSets));
        logger.debug("MatchSources.handleMatch selectedUsers:\n"  + SidUidTuple.ToStrings(selectedUsers));

        const errors: string[] = this.checkErrors(selectedUsers);
        if (errors == null) {
            MatchHelper.getMatchSourcesAsync(this.props.localAnswerRepos, this.props.answerRepos, selectedUsers)
                .then(sources => {
                    logger.debug("MatchSources.handleMatch localsource: " + MatchSource.ToString(sources.local));
                    logger.debug("MatchSources.handleMatch othersource:\n" + MatchSource.ToStrings(sources.others));
                    return sources;
                })
                .then(sources => this.props.matcher.getMatchesAsync(sources.local, sources.others))
                .then(results => this.props.matchRepos.createMatchesCleaningAsync(results))
                .then(ok => {
                    this.props.matchRepos.getMatchesAsync(sid)
                        .then(matches => logger.debug("MatchSources.handleMatch matches:\n" + MatchResult.ToStrings(matches)))
                        .catch(reason => "MatchSources.handleMatch reason: " + reason);
                    return ok;
                })
                .then(ok => this.props.routeProps.history.push(resultsUrl)) //todo
                .catch(reason => logger.error("reason: " + reason));
        }
        else {
            //TODO: store error in state
            logger.error("MatchSources.handleMatch errors: " + errors);
        }

    }

    private checkErrors(users: SidUidTuple[]): string[] {
        //TODO
        const hasUsers: boolean = users && (users.length > 0);
        return (hasUsers)
            ? null
            : ["no users selected"];
    }

    private isSelected(uid: number): boolean {
        return this.state.selection.some(id => id == uid);
    }

    public render() {
        const sid = this.props.routeProps.match.params["sid"];
        let table = this.state.loading
            ? this.renderLoading()
            : this.renderTable(this.state.answerSets, this.state.users);
        return <div>
            <h1>Survey {sid} / Match Partners</h1>
            {table}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Answers...</em></p >;
    }

    private renderTable(answerSets: SurveyUser[], users: User[]) {
        const myUrl = this.props.routeProps.match.url;
        const sid: number = this.props.routeProps.match.params["sid"];
        const allSelected: boolean = this.state.selectAll;
        const userName = (uid) => users
            .filter(u => u.id == uid)
            .map(u => u.name)[0];
        const sortedSets = answerSets
            .slice()
            .sort((set1, set2) => set1.userId - set2.userId);
        return <div><table className='table'>
            <thead>
                <tr>
                    <th style={{ width: '1%' }}>
                        <input className="form-check-input" type="checkbox" name="allToggle" value="allVal"
                            checked={allSelected} onChange={(e) => this.handleToggleAll(e)}></input>
                    </th>
                    <th style={{ width: '1%' }}>sid</th>
                    <th style={{ width: '1%' }}>uid</th>
                    <th>link</th>
                </tr>
            </thead>
            <tbody>
                {sortedSets.map(suid =>
                    <tr key={suid.userId}>
                        <td>
                            <input className="form-check-input" type="checkbox" name="itemToggle" value="itemVal"
                                checked={this.isSelected(suid.userId)} onChange={(e) => this.handleToggleItem(e, suid.userId)}></input>
                        </td>
                        <td>{suid.surveyId}</td>
                        <td>{suid.userId}</td>
                        <td>
                            <Link to={`${myUrl}/${suid && suid.userId}`}>{userName(suid.userId)}</Link>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>
        <button className="" onClick={e => this.handleMatch(e, sid)}>Match</button>
        </div>
        ;
    }
}