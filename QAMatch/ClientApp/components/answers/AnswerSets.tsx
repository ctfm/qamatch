﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import 'isomorphic-fetch';
import { Answer } from '../../services/answers/Answer';
import { IAnswerRepos } from '../../services/answers/IAnswerRepos';
import { Question } from '../../services/questions/Question';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import { SurveyUser } from './SurveyUser';
import { DOMElement } from 'react';
import { IUserRepos } from '../../services/users/IUserRepos';
import { User } from '../../services/users/User';

interface AnswersProps {
    routeProps: RouteComponentProps<{}>;
    userRepos: IUserRepos;
    answerRepos: IAnswerRepos;
    questionRepos: IQuestionRepos;
}

interface AnswersState {
    answerSets: SurveyUser[];
    loading: boolean;
}

export class AnswerSets extends React.Component<AnswersProps, AnswersState> {
    constructor(props: AnswersProps) {
        super(props);
        this.state = {
            answerSets: [],
            loading: true,
        };
    }

    public componentDidMount() {
        //const uid: number = this.props.routeProps.match.params["uid"];
        const sid: number = this.props.routeProps.match.params["sid"];
        this.fetchAndSetQandas(sid);
    }

    private fetchAndSetQandas(sid: number) {
        const userPromise = null; //todo
        const answersPromise = this.props.answerRepos.getUidsBySidAsync(sid)
            .then(uids => this.props.userRepos.getUsersByIdsAsync(uids))
            .then(users => this.mergeUids(sid, users))
            .then(data => this.setState((prev, props) => {
                return { loading: false, answerSets: data };
            }))
            .catch(reason => console.log("no uids reason: " + reason));
    }

    private mergeUids(sid: number, users: User[]): SurveyUser[] {
        if (sid && users) {
            const result: SurveyUser[] = users
                .map(user => { return { surveyId: sid, user: user }; });
            return result;
        }
        return [];
    }

    public render() {
        const uid = this.props.routeProps.match.params["uid"];
        const sid = this.props.routeProps.match.params["sid"];
        let table = this.state.loading
            ? this.renderLoading()
            : this.renderTable(this.state.answerSets);
        return <div>
            <h2>Survey {sid} / Users  {uid}</h2>
            {table}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Answers...</em></p >;
    }

    private renderTable(answerSets: SurveyUser[]) {
        const myUrl = this.props.routeProps.match.url;
        const sortedSets = answerSets
            .slice()
            .sort((set1, set2) => set1.user.id - set2.user.id);
        return <table className='table'>
            <thead>
                <tr>
                    <th style={{ width: '1%' }}>sid</th>
                    <th style={{ width: '1%' }}>uid</th>
                    <th>link</th>
                </tr>
            </thead>
            <tbody>
                {sortedSets.map(suid =>
                    <tr key={suid.user.id}>
                        <td>{suid.surveyId && suid.surveyId }</td>
                        <td>{suid.user && suid.user.id}</td>
                        <td>
                            <Link to={`${myUrl}/${suid.user && suid.user.id}`}>{suid.user && suid.user.name}</Link>
                        </td>
                    </tr>
                )}
            </tbody>
        </table>;
    }
}

