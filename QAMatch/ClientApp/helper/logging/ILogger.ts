export interface ILogger {
    debug(message: string);
    info(message: string);
    warn(message: string);
    error(message: string);
}