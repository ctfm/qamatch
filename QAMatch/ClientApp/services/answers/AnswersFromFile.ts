﻿import { IAnswerRepos } from "./IAnswerRepos";
import { Answer } from "./Answer";
import dataS1U01 from "./data/answersS1U01.json";
import dataS1U02 from "./data/answersS1U02.json";
import dataS1U10 from "./data/answersS1U10BayernCSU.json";
import dataS1U11 from "./data/answersS1U11BayernGruene.json";

export class AnswersFromFile implements IAnswerRepos {

    getUidsBySidAsync(sid: number): Promise<number[]> {
        const uidsList = this.AnswerList
            .filter(answer => answer.surveyId == sid)
            .map(answer => answer.userId);
        const uids: number[] = Array.from(new Set(uidsList));
        return new Promise<number[]>((resolve, reject) => resolve(uids));
    }

    getAnswersBySidAsync(sid: number): Promise<Answer[]> {
        const answers = this.AnswerList
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    getAnswersByUSidAsync(uid: number, sid: number): Promise<Answer[]> {
        const answers = this.AnswerList
            .filter(answer => uid && answer.userId == uid)
            .filter(answer => sid && answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    getAnswerById(id: number): Promise<Answer> {
        const answer = this.findAnswerById(id);
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise;
    }

    getAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<Answer> {
        const answer: Answer = this.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid)
            .filter(answer => answer.questionId == qid)[0];
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise
    }

    createAnswerAsync(answer: Answer): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    updateAnswerAsync(answer: Answer): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    deleteAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    private AnswerList: Answer[];

    constructor() {
        console.log("AnswersFromFile data1: " + (dataS1U01 && dataS1U01.length));
        console.log("AnswersFromFile data2: " + (dataS1U02 && dataS1U02.length));
        console.log("AnswersFromFile data10: " + (dataS1U10 && dataS1U10.length));
        console.log("AnswersFromFile data11: " + (dataS1U11 && dataS1U11.length));
        this.AnswerList = dataS1U01
            .concat(dataS1U02)
            .concat(dataS1U10)
            .concat(dataS1U11);
        console.log("AnswersFromFile answers: " + this.AnswerList.length);
    }

    private findAnswerById(aid: number) {
        return this.AnswerList
            .filter(ans => ans.id == aid)[0] || null;
    }
}