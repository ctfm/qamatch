﻿import { IMatchRepos } from "./IMatchRepos";
import { MatchResult } from "./MatchResult";

export class LocalMatchRepos implements IMatchRepos {

    public getMatchesAsync(sid: number): Promise<MatchResult[]> {
        const matches = this.matches
            .filter(match => match.sid == sid);
        return new Promise<MatchResult[]>((resolve, reject) => {
            if (matches) resolve(matches);
            else reject("no data");
        });
    }

    public getMatchesByUidAsync(sid: number, uid1: number, uid2: number): Promise<MatchResult> {
        const match: MatchResult = this.matches
            .filter(match => (match.sid == sid && match.uid1 == uid1 && match.uid2 == uid2))
            [0];
        return new Promise<MatchResult>((resolve, reject) => {
            if (match) resolve(match);
            else reject("no data");
        });
    }

    public createMatchAsync(match: MatchResult): Promise<boolean> {
        let done: boolean = false;
        if (!this.findByMatch(match)) {
            this.matches = this.matches.concat(match);
            done = true;
        } else {
            done = false;
        }
        return new Promise<boolean>((resolve, reject) => {
            if (done) resolve(true);
            else reject("already exists in repos");
        });
    }

    public createMatchesAsync(matches: MatchResult[]): Promise<boolean> {
        let done: boolean = false;
        const anyFound = matches.filter(match => this.findByMatch(match))[0];
        if (!anyFound) {
            this.matches = this.matches.concat(matches);
            done = true;
        } else {
            done = false;
        }
        return new Promise<boolean>((resolve, reject) => {
            if (done) resolve(true);
            else reject("already existing in repos");
        });
    }

    public createMatchesCleaningAsync(matches: MatchResult[]): Promise<boolean> {
        this.matches = matches;
        return new Promise<boolean>((resolve, reject) => resolve(true));
    }

    public updateMatchAsync(match: MatchResult): Promise<boolean> {
        //TODO
        return null;
    }

    public deleteMatchAsync(id: number): Promise<boolean> {
        //TODO
        return null;
    }

    public deleteMatchesAsync(sid: number): Promise<boolean> {
        //TODO
        return null;
    }

    public deleteAllMatchesAsync(): Promise<boolean> {
        this.matches = [];
        return new Promise<boolean>((resolve, reject) => resolve(true));
    }


    private findBySid(sid: number): MatchResult[] {
        return this.matches
            .filter(rec => rec.sid == sid);
    }

    private findByMatch(match: MatchResult): MatchResult {
        const matchFound = this.matches
            .filter(record => record.sid == match.sid)
            .filter(record => (record.uid1 == match.uid1) && (record.uid2 == match.uid2))[0] || null;
        return matchFound;
    }

    private matches: MatchResult[];

    constructor() {
        this.matches = [];
    }
}