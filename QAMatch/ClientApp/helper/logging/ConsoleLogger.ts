import { ILogger } from "./ILogger";

export class ConsoleLogger implements ILogger {

    private minLevel: number;

    constructor(minLevel: number) {
        this.minLevel = minLevel;   
    }

    public debug(message: string) {
        (this.minLevel <= 1) && console.log(message);
    }    
    
    public info(message: string) {
        (this.minLevel <= 2) && console.log(message);
    }

    public warn(message: string) {
        (this.minLevel <= 3) && console.log(message);
    }

    public error(message: string) {
        (this.minLevel <= 4) && console.log(message);
    }


}