﻿import { Answer } from './Answer';
import { IAnswerRepos } from './IAnswerRepos';

export class AnswersInMemory implements IAnswerRepos {

    public getUidsBySidAsync(sid: number): Promise<number[]> {
        //TODO
        const uidsList = this.AnswerList
            .filter(answer => answer.surveyId == sid)
            .map(answer => answer.userId);
        const uids: number[] = Array.from(new Set(uidsList));
        //const uids: number[] = this.AnswerList
        //    .filter(answer => answer.surveyId == sid)
        //    .map(answer => answer.userId)
        //    .sort()
        //    .reduce((acc, curr) => this.sortedToUnique(acc, curr), []);
        return new Promise<number[]>((resolve, reject) => resolve(uids));
    }

    public getAnswersBySidAsync(sid: number): Promise<Answer[]> {
        const answers = this.AnswerList
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswersByUSidAsync(uid: number, sid: number): Promise<Answer[]> {
        const answers = this.AnswerList
            .filter(answer => uid && answer.userId == uid)
            .filter(answer => sid && answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswerById(id: number): Promise<Answer> {
        const answer = this.findAnswerById(id);
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise;
    }

    public getAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<Answer> {
        const answer: Answer = this.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid)
            .filter(answer => answer.questionId == qid)[0];
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise
    }

    public createAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (!this.findAnswerById(answer.id)) {
            const id = AnswersInMemory.nextId();
            const newAnswer = { ...answer, id: id };
            this.AnswerList = this.AnswerList.concat(newAnswer);
            done = true;
        }
        const createPromise = new Promise<boolean>((resolve, reject) => {
            if (done) resolve(done)
            else reject("not done: answer: " + Answer.toString(answer))
        });
        return createPromise;
    }

    public updateAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (this.findAnswerById(answer.id)) {
            this.AnswerList = this.AnswerList
                .map(item => (item.userId == answer.userId && item.id == answer.id) ? answer : item);
            done = true;
        }
        const updatePromise = new Promise<boolean>((res, rej) => {
            if (done) res(done)
            else rej("not done: answer: " + Answer.toString(answer));
        });
        return updatePromise;
    }

    public deleteAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }


    private AnswerList: Answer[];
    private static LastId: number = 0;

    constructor() {
        //TODO
        this.AnswerList = []
            .concat(this.createAnswersS1U1())
            .concat(this.createAnswersS1U2())
            .concat(this.createAnswersS1U3())
            .concat(this.createAnswersS2U1())
            ;
    }

    private static nextId(): number {
        AnswersInMemory.LastId = AnswersInMemory.LastId + 1;
        return AnswersInMemory.LastId;
    }

    private createAnswersS1U1() {
        const sid = 1;
        const uid = 1;
        const answers: Answer[] = [
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 1, choices: {}, weight: 2, weightRange: [0, 4], scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 2, choices: {}, weight: 2, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 3, choices: {}, weight: 2, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 4, choices: {}, weight: 2, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 5, choices: {}, weight: 2, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] }
        ];
        return answers;
    }

    private createAnswersS1U2() {
        const sid = 1;
        const uid = 2;
        const answers: Answer[] = [
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 1, choices: {}, weight: 3, weightRange: [0, 4],  scale: -2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 2, choices: {}, weight: 3, weightRange: [0, 4],  scale: -2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 3, choices: {}, weight: 3, weightRange: [0, 4],  scale: -2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 4, choices: {}, weight: 3, weightRange: [0, 4],  scale: -2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 5, choices: {}, weight: 3, weightRange: [0, 4],  scale: -2, scaleRange: [-2, 2] }
        ];
        return answers;
    }

    private createAnswersS1U3() {
        const sid = 1;
        const uid = 3;
        const answers: Answer[] = [
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 1, choices: {}, weight: 3, weightRange: [0, 4],  scale: 2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 2, choices: {}, weight: 3, weightRange: [0, 4],  scale: 2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 3, choices: {}, weight: 3, weightRange: [0, 4],  scale: 2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 4, choices: {}, weight: 3, weightRange: [0, 4],  scale: 2, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 5, choices: {}, weight: 3, weightRange: [0, 4],  scale: 2, scaleRange: [-2, 2] }
        ];
        return answers;
    }

    private createAnswersS2U1() {
        const sid = 2;
        const uid = 1;
        const answers: Answer[] = [
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 6, choices: {}, weight: 4, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 7, choices: {}, weight: 4, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 8, choices: {}, weight: 4, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 9, choices: {}, weight: 4, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] },
            { id: AnswersInMemory.nextId(), surveyId: sid, userId: uid, questionId: 10, choices: {}, weight: 4, weightRange: [0, 4],  scale: 0, scaleRange: [-2, 2] }
        ];
        return answers;
    }

    private findAnswerById(aid: number) {
        return this.AnswerList
            .filter(ans => ans.id == aid)[0] || null;
    }

    private sortedToUnique(acc: number[], curr: number): number[] {
        //TODO
        const result = (acc.length === 0 || acc[length - 1] !== curr)
            ? acc.concat(curr)
            : acc;
        return result;
    }


}