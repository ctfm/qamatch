﻿import { MatchSource } from "./MatchSource";

export class MatchSourcesTuple {
    local: MatchSource;
    others: MatchSource[];
}