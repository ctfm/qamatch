# QAMatch
# [1.12.0.2]

# requirements:
- npm
- dotnet core

# start:
- npm install (optional?)
- dotnet build
- dotnet bin\Debug\netcoreapp2.0\QAMatch.dll
