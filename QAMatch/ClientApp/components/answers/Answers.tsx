﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import 'isomorphic-fetch';
import { User } from '../../services/users/User';
import { IUserRepos } from '../../services/users/IUserRepos';
import { Answer } from '../../services/answers/Answer';
import { IAnswerRepos } from '../../services/answers/IAnswerRepos';
import { Question } from '../../services/questions/Question';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import { QAndA } from './QAndA';
import { DOMElement } from 'react';
import { ILogger } from '../../helper/logging/ILogger';
import { Survey } from '../../services/surveys/Survey';
import { ISurveyRepos } from '../../services/surveys/ISurveyRepos';
import { UrlHelper } from '../../helper/url/UrlHelper';

interface AnswersProps {
    routeProps: RouteComponentProps<{}>;
    logger: ILogger;
    userRepos: IUserRepos;
    surveyRepos: ISurveyRepos;
    answerRepos: IAnswerRepos;
    questionRepos: IQuestionRepos;
}

interface AnswersState {
    user: User;
    survey: Survey;
    qandas: QAndA[];
    loading: boolean;
}

export class Answers extends React.Component<AnswersProps, AnswersState> {

    constructor(props: AnswersProps) {
        super(props);
        this.state = {
            user: null,
            survey: null,
            qandas: [],
            loading: true,
        };
    }

    public componentDidMount() {
        const uid: number = this.props.routeProps.match.params["uid"] || 0;
        const sid: number = this.props.routeProps.match.params["sid"];
        this.fetchAndSetQandas(uid, sid);
    }

    private fetchAndSetQandas(uid: number, sid: number) {
        const logger = this.props.logger;
        const userPromise = this.props.userRepos.getUserByIdAsync(uid);
        const surveyPromise = this.props.surveyRepos.getSurveyByIdAsync(sid);
        const questionsPromise = this.props.questionRepos.getQuestionsAsync(sid);
        const answersPromise = this.props.answerRepos.getAnswersByUSidAsync(uid, sid);
        Promise.all([userPromise, surveyPromise, questionsPromise, answersPromise])
            .then(data => this.mergeData(data[0], data[1], data[2], data[3]))
            .then(data => {
                logger.info("Answers.fetchAndSetQandas: user:        " + User.toString(data[0]));
                logger.info("Answers.fetchAndSetQandas: survey:      " + Survey.toString(data[1]));
                logger.info("Answers.fetchAndSetQandas: question[0]: " + Question.ToString(data[2] && data[2][0].question));
                logger.info("Answers.fetchAndSetQandas: answer[0]:   " + Answer.toString(data[2] && data[2][1].answer));
                return data;
            })
            .then(data => this.setState((prev, props) => {
                return {loading: false, user: data[0], survey: data[1], qandas: data[2]}
            }))
            .catch(reason => logger.error("no qandas reason: " + reason));
    }

    private mergeData(user: User, survey: Survey, questions: Question[], answers: Answer[]): [User,Survey,QAndA[]] {
        const qandas: QAndA[] = questions
            .map(question => {
                const answer = answers.filter(answer => answer.questionId == question.id)[0] || null;
                const qanda: QAndA = question && { question, answer };
                return qanda;
            })
            .filter(qanda => qanda != null);
        const data: [User, Survey, QAndA[]] = [user, survey, qandas];
        return data;
    }

    public render() {
        const sid = this.props.routeProps.match.params["sid"];
        const isLocal: boolean = (this.props.routeProps.match.params["uid"]) ? false : true;
        const surveyName = this.state.survey && this.state.survey.title;
        const userName = (isLocal)
            ? "local"
            : (this.state.user && this.state.user.name);
        let table = this.state.loading
            ? this.renderLoading()
            : this.renderTable(this.state.qandas, isLocal);
        return <div>
            <h2>{surveyName} / {userName}</h2>
            <p>.</p>
            {table}
            {(isLocal) && <Link to={UrlHelper.getMatchSources(sid)} className="btn">Match</Link>}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Answers...</em></p >;
    }

    private renderTable(qandas: QAndA[], isLocal: boolean) {
        const sid = this.props.routeProps.match.params["sid"];
        const sortedQandas = qandas
            .slice()
            .sort((qa1, qa2) => qa1.question.order - qa2.question.order);
        return <div><table className='table'>
            <thead>
                <tr>
                    <th style={{ width: '1%' }}>qid</th>
                    <th style={{ width: '1%' }}>order</th>
                    <th style={{ width: '70%' }}>Title</th>
                    <th>Weight</th>
                    <th>Value</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {sortedQandas.map(qanda =>
                    <tr key={qanda.question.id}>
                        <td>{qanda.question.id}</td>
                        <td>{qanda.question.order}</td>
                        <td>
                            <Link to={UrlHelper.getLocalAnswersQuestionDetail(sid, qanda.question.id)}>{qanda.question.title}</Link>
                        </td>
                        <td>{qanda.answer && qanda.answer.weight}</td>
                        <td>{qanda.answer && qanda.answer.scale}</td>
                        <td>{(isLocal) && ((qanda.answer)
                            ? <Link to={UrlHelper.getLocalAnswersQuestionAnswer(sid, qanda.question.order)}>Change</Link>
                            : <Link to={UrlHelper.getLocalAnswersQuestionAnswer(sid, qanda.question.order)}>Answer</Link>)
                        }
                        </td>
                    </tr>
                )}
            </tbody>
        </table>
        </div>;
    }
}
