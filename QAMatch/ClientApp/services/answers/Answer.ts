﻿export class Answer {
    id: number;
    questionId: number;
    surveyId: number;
    userId: number;
    choices: {} | null;
    scale: number | null;
    scaleRange: [number, number] | null;
    weight: number | null;
    weightRange: [number, number] | null;

    public static toString(answer: Answer): string {
        if (answer != null) {
            return "id: " + answer.id
                + " | qid: " + answer.questionId
                + " | uid: " + answer.userId
                + " | weight: " + answer.weight
                + " | scale: " + answer.scale
                + " | range: " + (answer.scaleRange && (answer.scaleRange[0] + " .. " + answer.scaleRange[1]))
                ;
        }
        return null;
    }
}