﻿import * as React from "react";
import { RouteComponentProps } from "react-router";
import { User } from "../../services/users/User";
import { IUserRepos } from "../../services/users/IUserRepos";

interface UsersProps {
    routeProps: RouteComponentProps<{}>;
    userRepos: IUserRepos;
}

interface UsersState {
    loading: boolean;
    users: User[];
}

export class Users extends React.Component<UsersProps, UsersState> {

    constructor(props: UsersProps) {
        super(props);
        this.state = {
            loading: true,
            users: []
        };
    }

    public componentDidMount() {
        this.fetchAndSetData();
    }

    private fetchAndSetData() {
        this.props.userRepos.getUsersAsync()
            .then(users => this.setState({
                loading: false,
                users: users
            }))
            .catch((reason) => console.log("reason: " + reason));
    }

    public render() {
        const users = this.state.users;
        const content = (this.state.loading)
            ? this.renderLoading()
            : this.renderContent(users);
        return <div>
            <h1>Users</h1>
            {content}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Users...</em></p>;
    }

    private renderContent(users: User[]) {
        //TODO
        return <div>
            <table className="table">
                <thead>
                    <tr>
                        <th style={{ width: '10%' }}>Id</th>
                        <th style={{ width: '20%' }}>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map(user =>
                        <tr key={user.id}>
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.description}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>;
    }

}