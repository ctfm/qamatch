﻿export class DeltaTuple {
    delta: number;
    weight: number;
}