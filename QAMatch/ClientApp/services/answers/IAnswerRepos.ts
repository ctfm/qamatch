﻿import { Answer } from './Answer';

export interface IAnswerRepos {
    getUidsBySidAsync(sid: number): Promise<number[]>;
    getAnswersBySidAsync(sid: number): Promise<Answer[]>;
    getAnswersByUSidAsync(uid: number, sid: number): Promise<Answer[]>;
    getAnswerById(id: number): Promise<Answer>;
    getAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<Answer>;

    createAnswerAsync(answer: Answer): Promise<boolean>;

    updateAnswerAsync(answer: Answer): Promise<boolean>;

    deleteAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<boolean>;
}