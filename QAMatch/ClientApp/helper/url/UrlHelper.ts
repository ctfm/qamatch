﻿import { RouteComponentProps } from "react-router";

export class UrlHelper {

    public static readonly surveys = "surveys";  
    public static readonly answersets = "answersets";
    public static readonly localanswers = "localanswers";
    public static readonly questions = "questions";
    public static readonly matchsources = "matchsources";
    public static readonly matchresults = "matchresults";

    public static getSurveyUrl(sid: number) {
        return `/${UrlHelper.surveys}/${sid}`;
    }

    public static getSurveyQuestions(sid: number) {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.questions}`;
    }

    public static getSurveyQuestionDetail(sid: number, qid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.questions}/${qid}/detail`;
    }

    public static getAnswersetUrl(sid: number, uid: number) {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.answersets}/${uid}`;
    }

    public static getLocalAnswersUrl(sid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.localanswers}`;
    }

    public static getLocalAnswersQuestionDetail(sid: number, qid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.localanswers}/question/${qid}/detail`;
    }

    public static getLocalAnswersQuestionAnswer(sid: number, qix: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.localanswers}/question/${qix}/answer`;
    }

    public static getMatchSources(sid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.matchsources}`;
    }

    public static getMatchResultsUrl(sid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.matchresults}`;
    }

    public static getMatchResultsUrl2(sid: number, uid: number): string {
        return `/${UrlHelper.surveys}/${sid}/${UrlHelper.matchresults}/${uid}`;
    }

    public static getLocalAnswersRoute(routeProps: RouteComponentProps<{}>): string {
        const sid = routeProps.match.params["sid"];
        return this.getLocalAnswersUrl(sid);
    }
}