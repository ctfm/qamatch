﻿import { IUserRepos } from "./IUserRepos";
import { User } from "./User";
import { AnswerResults } from "../../components/match/AnswerResults";

export class UsersInMemory implements IUserRepos {

    public getUsersAsync(): Promise<User[]> {
        return new Promise<User[]>((resolve, reject) => {
            resolve(this.UserList);
        });
    }

    public getUsersByIdsAsync(ids: number[]): Promise<User[]> {
        //TODO
        const users: User[] = this.UserList
            .filter(user => ids.some(id => id == user.id));
        return new Promise<User[]>((resolve, reject) => {
            resolve(users);
        });
    }

    public getUserByIdAsync(id: number): Promise<User> {
        const user: User = this.UserList.find(user => user.id == id);
        return new Promise<User>((resolve, reject) => {
            resolve(user);
        });
    }

    private UserList: User[];

    constructor() {
        this.UserList = UsersInMemory.createUsers();
    }

    private static createUsers(): User[] {
        const users: User[] = [
            { id: 1, name: "ABC", description: "desc 01" },
            { id: 2, name: "DEF", description: "desc 02" },
            { id: 3, name: "GHI", description: "desc 03" },
            { id: 4, name: "JKL", description: "desc 04" },
            { id: 5, name: "MNO", description: "desc 05" },
            { id: 10, name: "CSU", description: "Chistlich Soziale Union"},
            { id: 11, name: "Grüne", description: "Bündnis 90 die Grünen" }
        ];
        return users;
    }
}