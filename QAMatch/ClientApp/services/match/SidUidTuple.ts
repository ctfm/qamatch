﻿export class SidUidTuple {
    surveyId: number;
    userId: number;

    public static ToString(tup: SidUidTuple): string {
        if (tup != null) {
            return "sid: " + tup.surveyId
                + " | uid: " + tup.userId
                ;
        }
        return null;
    }

    public static ToStrings(results: SidUidTuple[]): string {
        if (results != null && results.length > 0) {
            return results
                .map(result => SidUidTuple.ToString(result))
                .reduce((acc, item) => acc + "\n" + item);
        }
        return null;
    }
}