﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { ILogger } from '../../helper/logging/ILogger';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import { IMatchRepos } from '../../services/match/IMatchRepos';
import { MatchResult } from '../../services/match/MatchResult';
import { UrlHelper } from '../../helper/url/UrlHelper';
import { AnswerTuple } from '../../services/match/AnswerTuple';
import { Question } from '../../services/questions/Question';
import { AnswerHelper } from '../answers/AnswerHelper';
import { User } from '../../services/users/User';
import { IUserRepos } from '../../services/users/IUserRepos';

interface MatchProps {
    routeProps: RouteComponentProps<{}>;
    logger: ILogger;
    userRepos: IUserRepos;
    matchRepos: IMatchRepos;
    questionRepos: IQuestionRepos;
}

interface MatchState {
    loading: boolean;
    user: User;
    match: MatchResult;
    questions: Question[];
}

export class AnswerResults extends React.Component<MatchProps, MatchState> {

    constructor(props: MatchProps) {
        super(props);
        this.state = {
            loading: true,
            user: null,
            match: null,
            questions: []
        };
    }

    public componentDidMount() {
        const sid: number = this.props.routeProps.match.params["sid"];
        const uid1: number = 0;
        const uid2: number = this.props.routeProps.match.params["uid"];
        this.fetchAndSetMatch(sid, uid1, uid2);
    }

    private fetchAndSetMatch(sid: number, uid1: number, uid2: number) {
        const logger = this.props.logger;
        const userPromise = this.props.userRepos.getUserByIdAsync(uid2);
        const matchPromise = this.props.matchRepos.getMatchesByUidAsync(sid, uid1, uid2);
        const questionsPromise = this.props.questionRepos.getQuestionsAsync(sid);
        Promise.all([userPromise, matchPromise, questionsPromise])
            .then(data => {
                const user: User = data[0];
                const match: MatchResult = data[1];
                const question0: Question = data && data[2] && data[2][0];
                logger.debug("MatchResults.fetch user:\n" + User.toString(user));
                logger.debug("MatchResults.fetch match:\n" + MatchResult.ToString(match));
                logger.debug("MatchResults.fetch.question[0]:\n" + Question.ToString(question0));
                return data;
            })
            .then(data => this.setState((prev, props) => {
                return { loading: false, user: data[0], match: data[1], questions: data[2] };
            }))
            .catch(reason => logger.error("MatchResults.fetch reason: " + reason));
    }

    public render() {
        const sid = this.props.routeProps.match.params["sid"];
        const uid = this.props.routeProps.match.params["uid"];
        const userName = this.state.user && this.state.user.name;
        let table = this.state.loading
            ? this.renderLoading()
            : this.renderData(this.state.user, this.state.match, this.state.questions);
        return <div>
            <h1>Survey {sid}: {userName} - me</h1>
            {table}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Matches...</em></p >;
    }

    private renderData(user: User, match: MatchResult, questions: Question[]) {
        const keyFunc = (delta: AnswerTuple) => delta.aid1 + "-" + delta.aid2;
        const deltas = match && match.deltaTups || [];
        const getOrder = (qid) => questions
            .filter(q => q.id == qid)
            .map(q => q.order)[0];
        const getTitle = (qid) => questions
            .filter(q => q.id == qid)
            .map(q => q.title)[0];
        const totalWeight: number = match && match.statistics && match.statistics.totalWeight;
        const totalWeightedDelta: number = match && match.statistics && match.statistics.totalDistance;
        const totalDelta: number = deltas
            .map(delta => delta.delta)
            .reduce((acc, delta) => acc + delta, 0);
        const maxDelta: number = match && match.statistics && match.statistics.maxDistance;
        const averageDelta: number = match && match.statistics && match.statistics.averageDistance;
        const score: number = match && match.statistics && match.statistics.scorePercent;
        const percentDelta: number = 100 - score;
        return <div>
            <table className='table'>
                <thead>
                    <tr>
                        <th style={{ width: '3%' }}>#</th>
                        <th>question</th>
                        <th style={{ width: '8%' }}>party</th>
                        <th style={{ width: '8%' }}>me</th>
                        <th style={{ width: '8%' }}>weight</th>
                        <th style={{ width: '8%' }}>delta</th>
                    </tr>
                </thead>
                <tbody>
                    {deltas.map(deltaTup =>
                        <tr key={keyFunc(deltaTup)}>
                            <td>{getOrder(deltaTup.qid)}</td>
                            <td>{getTitle(deltaTup.qid)}</td>
                            <td>{deltaTup.scale2}</td>
                            <td>{deltaTup.scale1}</td>
                            <td className={AnswerHelper.getWeightTDColor(deltaTup.weight)}>{deltaTup.weight}</td>
                            <td className={AnswerHelper.getDeltaTDColor(deltaTup.delta, deltaTup.weight)}>{deltaTup.delta}</td>
                        </tr>
                    )}
                </tbody>
            </table>
            <table className='table'>
                <thead>
                    <tr>
                        <th style={{ width: '1%' }}></th>
                        <th></th>
                        <th style={{ width: '8%' }}></th>
                        <th style={{ width: '8%' }}></th>
                        <th style={{ width: '8%' }}></th>
                        <th style={{ width: '8%' }}></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>total</td>
                        <td>{totalWeight}</td>
                        <td>{totalWeightedDelta} ({totalDelta})</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>delta</td>
                        <td>max {maxDelta}</td>
                        <td>{averageDelta && averageDelta.toFixed(2)}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>delta%</td>
                        <td></td>
                        <td>{percentDelta && Math.round(percentDelta)}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>score%</td>
                        <td></td>
                        <td>{score && Math.round(score)}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        ;
    }
}