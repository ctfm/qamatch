﻿import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './Layout';
import { Home } from './Home';
import { ILogger } from '../helper/logging/ILogger';
import { ConsoleLogger } from '../helper/logging/ConsoleLogger';
//for surveys:
import { Surveys } from './surveys/Surveys';
import { ISurveyRepos } from '../services/surveys/ISurveyRepos';
import { SurveysInMemory } from '../services/surveys/SurveysInMemory';
//for questions:
import { Questions } from './questions/Questions';
import { QuestionForm } from './questions/QuestionForm';
import { QuestionDetail } from './questions/QuestionDetail';
import { IQuestionRepos } from '../services/questions/IQuestionRepos';
import { QuestionsInMemory } from '../services/questions/QuestionsInMemory';
import { Question } from '../services/questions/Question';
//for answers:
import { Answers } from './answers/Answers';
import { AnswerSets } from './answers/AnswerSets';
import { IAnswerRepos } from '../services/answers/IAnswerRepos';
import { AnswersInMemory } from '../services/answers/AnswersInMemory';
//import { AnswersInSession } from '../services/answers/AnswersInSession';
//import { LocalAnswers } from '../services/answers/LocalAnswers';
import { LocalAnswersInSession } from '../services/answers/LocalAnswersInSession';
import { AnswerForm } from '../components/answers/AnswerForm';
import { Answer } from '../services/answers/Answer';
//for matches:
import { MatchSources } from '../components/match/MatchSources';
import { IMatcher } from '../services/match/IMatcher';
import { LocalMatcher } from '../services/match/LocalMatcher';
import { IMatchRepos } from '../services/match/IMatchRepos';
import { LocalMatchRepos } from '../services/match/LocalMatchRepos';
import { MatchResults } from './match/MatchResults';
import { AnswerResults } from './match/AnswerResults';
import { SurveyDetail } from './surveys/SurveyDetail';
import { Survey } from '../services/surveys/Survey';
import { Users } from './users/Users';
import { IUserRepos } from '../services/users/IUserRepos';
import { UsersInMemory } from '../services/users/UsersInMemory';
import { QuestionsFromFile } from '../services/questions/QuestionsFromFile';
import { AnswersFromFile } from '../services/answers/AnswersFromFile';

interface AppProps {
}

interface AppState {
    logger: ILogger;
    usersRepos: IUserRepos;
    surveysRepos: ISurveyRepos;
    questionsRepos: IQuestionRepos;
    globalAnswers: IAnswerRepos;
    localAnswers: IAnswerRepos; //todo
    localMatcher: IMatcher;
    localMatches: IMatchRepos;
}

export class App extends React.Component<AppProps, AppState>  {

    constructor(props: AppProps) {
        super(props);
        this.state = {
            logger: new ConsoleLogger(1),
            usersRepos: new UsersInMemory(),
            surveysRepos: new SurveysInMemory(),
            //questionsRepos: new QuestionsInMemory(),
            questionsRepos: new QuestionsFromFile(),
            //globalAnswers: new AnswersInMemory(),
            globalAnswers: new AnswersFromFile(),
            localAnswers: new LocalAnswersInSession(),
            localMatcher: new LocalMatcher(),
            localMatches: new LocalMatchRepos()
        };
    }

    public render() {
        const logger = this.state.logger;
        const userRepos = this.state.usersRepos;
        const surveyRepos = this.state.surveysRepos;
        const questionRepos = this.state.questionsRepos;
        const globalAnswers = this.state.globalAnswers;
        const localAnswers = this.state.localAnswers;
        const localMatcher = this.state.localMatcher;
        const localMatches = this.state.localMatches;

        return <Layout>
            <Route exact path='/' component={Home} />

            <Route exact path='/surveys' render={(routeProps) => <Surveys routeProps={routeProps} repos={surveyRepos} />} />
            <Route exact path='/surveys/:sid' render={(routeProps) => <SurveyDetail routeProps={routeProps} surveyRepos={surveyRepos} questionRepos={questionRepos} />} />

            <Route exact path='/users' render={(routeProps) => <Users routeProps={routeProps} userRepos={userRepos}/>} />

            <Route exact path='/surveys/:sid/questions' render={(routeProps) => <Questions routeProps={routeProps} repos={questionRepos} />} />
            <Route exact path='/surveys/:sid/questions/:qid/detail' render={(routeProps) => <QuestionDetail routeProps={routeProps} repos={questionRepos} />} />
            <Route exact path='/surveys/:sid/questions/create' render={(routeProps) => <QuestionForm routeProps={routeProps} repos={questionRepos} />} />
            <Route       path='/surveys/:sid/questions/:qid/edit' render={(routeProps) => <QuestionForm routeProps={routeProps} repos={questionRepos} />} />

            <Route exact path='/surveys/:sid/answersets' render={(routeProps) => <AnswerSets routeProps={routeProps} answerRepos={globalAnswers} questionRepos={questionRepos} userRepos={userRepos} />} />
            <Route exact path='/surveys/:sid/answersets/:uid' render={(routeProps) =>
                <Answers routeProps={routeProps} logger={logger} answerRepos={globalAnswers} questionRepos={questionRepos} userRepos={userRepos} surveyRepos={surveyRepos} />} />
            <Route       path='/surveys/:sid/answersets/:uid/question/:qid/detail' render={(routeProps) => <QuestionDetail routeProps={routeProps} repos={questionRepos} />} />

            <Route exact path='/surveys/:sid/localanswers' render={(routeProps) =>
                <Answers routeProps={routeProps} logger={logger} answerRepos={localAnswers} questionRepos={questionRepos} userRepos={userRepos} surveyRepos={surveyRepos} />} />
            <Route       path='/surveys/:sid/localanswers/question/:qid/detail' render={(routeProps) => <QuestionDetail routeProps={routeProps} repos={questionRepos} />} />
            <Route       path='/surveys/:sid/localanswers/question/:qix/answer' render={(routeProps) =>
                <AnswerForm key={routeProps.match.url} routeProps={routeProps} questionRepos={questionRepos} answerRepos={localAnswers} />
            } />

            <Route exact path='/surveys/:sid/matchsources' render={(routeProps) =>
                <MatchSources routeProps={routeProps} logger={logger} userRepos={userRepos}
                    answerRepos={globalAnswers} questionRepos={questionRepos} localAnswerRepos={localAnswers}
                    matchRepos={localMatches} matcher={localMatcher} />} />
            <Route exact path='/surveys/:sid/matchsources/:uid' render={(routeProps) =>
                <Answers routeProps={routeProps} logger={logger} answerRepos={globalAnswers} questionRepos={questionRepos} userRepos={userRepos} surveyRepos={surveyRepos} />} />
            <Route path='/surveys/:sid/matchsources/:uid/question/:qid/detail' render={(routeProps) =>
                <QuestionDetail routeProps={routeProps} repos={questionRepos} />} />

            <Route exact path='/surveys/:sid/matchresults' render={(routeProps) =>
                <MatchResults routeProps={routeProps} logger={logger} matchRepos={localMatches} userRepos={userRepos} />} />
            <Route exact path='/surveys/:sid/matchresults/:uid' render={(routeProps) =>
                <AnswerResults routeProps={routeProps} logger={logger} matchRepos={localMatches} userRepos={userRepos} questionRepos={questionRepos} />} />
        </Layout>;
    }
}