﻿export class DistanceValues {
    averageDistance: number;
    maxDistance: number;
    totalDistance: number;
    totalWeight: number;
    scorePercent: number;
}