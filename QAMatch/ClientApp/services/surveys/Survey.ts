﻿export class Survey {
    id: number;
    order: number;
    title: string;
    description: string;

    public static toString(survey: Survey): string {
        if (survey != null) {
            return "id: " + survey.id
                + " | order: " + survey.order
                + " | title: " + survey.title
                //+ " | description: " + survey.description
                ;
        }
        return null;
    }
}