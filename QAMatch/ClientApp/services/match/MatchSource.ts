﻿import { Answer } from "../answers/Answer";

export class MatchSource {
    surveyId: number;
    userId: number;
    answers: Answer[];

    public static ToString(source: MatchSource): string {
        const answers = source.answers && source.answers.length && source.answers.length.toString();
        if (source != null) {
            return "sid: " + source.surveyId
                + " | uid: " + source.userId
                + " | answers: " + answers
                ;
        }
        return null;
    }

    public static ToStrings(sources: MatchSource[]): string {
        if (sources != null && sources.length > 0) {
            return sources
                .map(result => MatchSource.ToString(result))
                .reduce((acc, item) => acc + "\n" + item);
        }
        return null;
    }
}