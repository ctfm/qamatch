﻿import { MatchResult } from "./MatchResult";
import { match } from "react-router";

export interface IMatchRepos {
    getMatchesAsync(sid: number): Promise<MatchResult[]>;
    getMatchesByUidAsync(sid: number, uid1: number, uid2: number): Promise<MatchResult>;

    createMatchAsync(match: MatchResult): Promise<boolean>;
    createMatchesAsync(matches: MatchResult[]): Promise<boolean>;
    createMatchesCleaningAsync(matches: MatchResult[]): Promise<boolean>;

    updateMatchAsync(match: MatchResult): Promise<boolean>;

    deleteMatchAsync(id: number): Promise<boolean>;
    deleteMatchesAsync(sid: number): Promise<boolean>;
    deleteAllMatchesAsync(): Promise<boolean>;
}