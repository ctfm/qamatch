﻿import { User } from "./User";

export interface IUserRepos {
    getUsersAsync(): Promise<User[]>;
    getUsersByIdsAsync(ids: number[]): Promise<User[]>;
    getUserByIdAsync(id: number): Promise<User>;
}