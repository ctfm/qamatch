﻿import { IAnswerRepos } from "../../services/answers/IAnswerRepos";
import { ILogger } from "../logging/ILogger";
import { Answer } from "../../services/answers/Answer";
import { MatchResult } from "../../services/match/MatchResult";
import { MatchSource } from "../../services/match/MatchSource";
import { SidUidTuple } from "../../services/match/SidUidTuple";
import { MatchSourcesTuple } from "../../services/match/MatchSourcesTuple";

export class MatchHelper {

    public static getMatchSourcesAsync(
        localAnswerRepos: IAnswerRepos, otherAnswersRepos: IAnswerRepos, answerSets: SidUidTuple[])
        : Promise<MatchSourcesTuple> {
        //TODO
        const sid = answerSets[0].surveyId;
        const localAnswerPromise = localAnswerRepos.getAnswersByUSidAsync(0, sid);
        const otherAnswersPromise = answerSets
            .map(answerSet => otherAnswersRepos.getAnswersByUSidAsync(answerSet.userId, answerSet.surveyId));
        const matchPromise = Promise.all(otherAnswersPromise)
            .then(otherAnswers => Promise.all([localAnswerPromise, otherAnswers]))
            .then(allAnswers => {
                const localSource = MatchHelper.toMatchSource(allAnswers[0]);
                const otherSources = MatchHelper.toMatchSources(allAnswers[1]);
                return { local: localSource, others: otherSources };
            });
        return matchPromise;
    }

    private static toMatchSources(answerSets: Answer[][]): MatchSource[] {
        const results = answerSets
            .map(answers => MatchHelper.toMatchSource(answers));
        return results;
    }

    private static toMatchSource(answers: Answer[]): MatchSource {
        const sid: number = answers && answers[0] && answers[0].surveyId;
        const uid: number = answers && answers[0] && answers[0].userId;
        const result: MatchSource = { surveyId: sid, userId: uid, answers: answers };
        return result;
    }
}