import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

export class Home extends React.Component<RouteComponentProps<{}>, {}> {
    public render() {
        return <div>
            <h1>QAMatch - Question Answer Matcher</h1>
            <p>Overview</p>
            <ul>
                <li>Overview of <a href='/surveys'>all surveys</a></li>
                <li>Overview of <a href='/users'>all users</a></li>
                <li>vote for <Link to='/surveys/1/localanswers'>Landtagswahl Bayern 2018</Link> and match with other users</li>
                <li>vote for <Link to='/surveys/2/localanswers'>Landtagswahl Hessen 2018</Link> and match with other users</li>
            </ul>
        </div>;
    }
}
