﻿import * as React from 'react';
import { PropTypes } from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import { Answer } from '../../services/answers/Answer';
import { IAnswerRepos } from '../../services/answers/IAnswerRepos';
import { Question } from '../../services/questions/Question';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import { AnswerHelper } from './AnswerHelper';
import { UrlHelper } from '../../helper/url/UrlHelper';
import { QRange } from '../../services/questions/QRange';

interface FormProps {
    routeProps: RouteComponentProps<{}>;
    questionRepos: IQuestionRepos;
    answerRepos: IAnswerRepos;
    key: string;
}

interface FormState {
    loading: boolean,
    firstQix: number,
    lastQix: number,
    question: Question,
    answer: Answer | null,
    formInput: {}
}

export class AnswerForm extends React.Component<FormProps, FormState> {

    constructor(props: FormProps) {
        super(props);
        this.state = {
            loading: true,
            firstQix: 1,
            lastQix: 10,   //check
            question: null,
            answer: null,
            formInput: {}
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleSaveAndNext = this.handleSaveAndNext.bind(this);
        this.handleSaveAndUp = this.handleSaveAndUp.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handleUp = this.handleUp.bind(this);
        this.handleWeight = this.handleWeight.bind(this);
        this.handleScale = this.handleScale.bind(this);
    }

    public componentDidMount() {
        const uid: number = 0; //todo
        const sid: number = this.props.routeProps.match.params["sid"];
        const qix: number = this.props.routeProps.match.params["qix"];
        if (sid != null && qix != null) {
            this.fetchAndSetQanda(sid, qix);
        }
    }

    private fetchAndSetQanda(sid: number, qix: number) {
        //TODO
        const qixRangePromise = null; //todo!
        this.props.questionRepos.getQuestionByIndexAsync(sid, qix)
            .then(question => {
                const answerPromise = this.props.answerRepos.getAnswerByUSQIdAsync(0, sid, question.id);
                const rangePromise = this.props.questionRepos.getQuestionsRangeAsync(sid);
                return Promise.all([question, answerPromise, rangePromise]);
            })
            .then(data => {
                console.log("AnswerForm.fetchAndSetQanda: sid: " + sid);
                console.log("AnswerForm.fetchAndSetQanda: qix: " + qix);
                console.log("AnswerForm.fetchAndSetQanda: question: " + Question.ToString(data[0]));
                console.log("AnswerForm.fetchAndSetQanda: answer:   " + Answer.toString(data[1]));
                console.log("AnswerForm.fetchAndSetQanda: range:   " + QRange.toString(data[2]));
                return data;
            })
            .then(data => this.setState((prev, props) => {
                return {
                    loading: false,
                    question: data[0],
                    answer: data[1],
                    firstQix: data[2].firstIndex || prev.firstQix,
                    lastQix: data[2].lastIndex || prev.lastQix
                };
            }))
            .catch(reason => console.log("fetchQanda: reason: " + reason));
    }

    private saveInputAsync(question: Question): Promise<boolean> {
        const aid = this.state.answer && this.state.answer.id;
        if (aid) {
            const oldAnswer = this.state.answer;
            const newAnswer: Answer = {
                ...oldAnswer,
                ...this.state.formInput
            };
            return this.props.answerRepos.updateAnswerAsync(newAnswer)
        }
        else {
            const qid = question.id;
            const sid = this.props.routeProps.match.params["sid"];
            const uid = this.props.routeProps.match.params["uid"] || 0;
            const oldAnswer = this.state.answer;
            const newAnswer: Answer = {
                ...oldAnswer,
                questionId: qid, surveyId: sid, userId: uid,
                weight: question.weight, weightRange: question.weightRange,
                scaleRange: question.scaleRange,
                ...this.state.formInput
            };
            return this.props.answerRepos.createAnswerAsync(newAnswer);
        }
    }

    private handleChange(event) {
        //TODO
        const name = event.target.name;
        const value = event.target.value;
        this.setState({
            formInput: { ...this.state.formInput, [name]: value }
        });
        console.log(""
            + "\nname: " + name + "| value: " + value
            + "\nstate.formInput.title: " + this.state.formInput['title']
            + "\nstate.formInput.order: " + this.state.formInput['order']
            + "\nstate.formInput.weight: " + this.state.formInput['weight']
            + "\nstate.formInput.scale: " + this.state.formInput['scale']
        );
    }

    private handleWeight(event, val: number) {
        this.setState({
            formInput: { ...this.state.formInput, weight: val }
        });
    }

    private handleScale(event, val: number) {
        this.setState({
            formInput: { ...this.state.formInput, scale: val }
        });
    }

    private handleSave(event, question: Question) {
        event.preventDefault();
        //const newAnswer: Answer = { ...this.state.answer, ...this.state.formInput };
        this.saveInputAsync(question)
            .then(ok => this.setState((prev, props) => {
                return { loading: false }  //todo: formInput: {}
            }))
            .catch(reason => console.log("AnswerForm.handleSave reason: " + reason));
    }

    private handleSaveAndNext(event, question: Question) {
        event.preventDefault();
        this.saveInputAsync(question)
            .then(ok => AnswerHelper.getQuestionUrl(this.props.routeProps.match.url, question.order, 1))
            .then(url => this.props.routeProps.history.push(url))
            .catch(reason => console.log("Answerform.handleSaveAndNext reason: " + reason));
    }

    private handleSaveAndUp(event, question: Question) {
        event.preventDefault();
        this.saveInputAsync(question)
            .then(ok => UrlHelper.getLocalAnswersRoute(this.props.routeProps))
            .then(url => this.props.routeProps.history.push(url))
            .catch(reason => console.log("Answerform.handleSaveAndUp reason: " + reason));
    }

    private handlePrev(event, question: Question) {
        event.preventDefault();
        const newUrl: string = AnswerHelper.getQuestionUrl(this.props.routeProps.match.url, question.order, -1);
        this.props.routeProps.history.push(newUrl);
    }

    private handleNext(event, question: Question) {
        event.preventDefault();
        const newUrl: string = AnswerHelper.getQuestionUrl(this.props.routeProps.match.url, question.order, 1);
        this.props.routeProps.history.push(newUrl);
    }

    private handleUp(event) {
        //TODO
        event.preventDefault();
        const newUrl = UrlHelper.getLocalAnswersRoute(this.props.routeProps);
        this.props.routeProps.history.push(newUrl);
    }

    public render() {
        const order = this.state.question && this.state.question.order;
        const first = this.state.firstQix;
        const last = this.state.lastQix;
        let contents = this.state.loading
            ? this.renderLoading()
            : this.renderForm(this.state.question, this.state.answer, first, last, this.state.formInput);
        return <div>
            <h1>Answer {order}/{last}</h1>
            {contents}
        </div>;
    }

    private renderLoading() {
        return <div>
            <p><em>Loading Answer Form ...</em></p>
            <p>route path: {this.props.routeProps.match.path}</p>
            <p>route params.sid: {this.props.routeProps.match.params["sid"]}</p>
            <p>route params.qix: {this.props.routeProps.match.params["qix"]}</p>
        </div>;
    }

    private renderForm(question: Question, answer: Answer, first: number, last: number, formInput: {}) {
        const hasChanged: boolean = formInput && Object.keys(formInput).length > 0;
        const weightVal: number = formInput['weight']
            || answer && answer.weight
            || question && question.weight;
        const scaleVal: number = formInput['scale']
            || answer && answer.scale;
        const answerId: string = answer && answer.id && answer.id.toString();

        return <form onSubmit={(e) => this.handleSave(e, question)}>
            <div className="form-group-row">
                <label className="control-label col-sm-3" htmlFor="title">Title</label>
                <div className="col-sm-9">
                    <input className="form-control" type="text" name="title" value={question.title} readOnly />
                </div>
            </div>
            <div className="form-group-row">
                <label className="control-label col-sm-3" htmlFor="description">Description</label>
                <div className="col-sm-9 mb-10">
                    <textarea className="form-control" name="description" value={question.description} rows={6} readOnly/>
                </div>
            </div>
            <div className="form-group-row">
                <label className="control-label col-sm-3" htmlFor="weight">Weight</label>
                <div className="col-sm-9 mb-10" role="group">
                    <button type="button" className={AnswerHelper.getWeightButtonStyle(0, weightVal)} onClick={(e) => this.handleWeight(e, 0)}>0</button>
                    <button type="button" className={AnswerHelper.getWeightButtonStyle(1, weightVal)} onClick={(e) => this.handleWeight(e, 1)}>1</button>
                    <button type="button" className={AnswerHelper.getWeightButtonStyle(2, weightVal)} onClick={(e) => this.handleWeight(e, 2)}>2</button>
                    <button type="button" className={AnswerHelper.getWeightButtonStyle(3, weightVal)} onClick={(e) => this.handleWeight(e, 3)}>3</button>
                    <button type="button" className={AnswerHelper.getWeightButtonStyle(4, weightVal)} onClick={(e) => this.handleWeight(e, 4)}>4</button>
                </div>
            </div>
            <div className="form-group-row">
                <label className="control-label col-sm-3" htmlFor="scale">Scale</label>
                <div className="col-sm-9 mb-10" role="group">
                    <button type="button" className={AnswerHelper.getScaleButtonStyle(-2, scaleVal)} onClick={(e) => this.handleScale(e, -2)}>NO</button>
                    <button type="button" className={AnswerHelper.getScaleButtonStyle(-1, scaleVal)} onClick={(e) => this.handleScale(e, -1)}>no</button>
                    <button type="button" className={AnswerHelper.getScaleButtonStyle(0, scaleVal)} onClick={(e) => this.handleScale(e, 0)}>neutral</button>
                    <button type="button" className={AnswerHelper.getScaleButtonStyle(1, scaleVal)} onClick={(e) => this.handleScale(e, 1)}>yes</button>
                    <button type="button" className={AnswerHelper.getScaleButtonStyle(2, scaleVal)} onClick={(e) => this.handleScale(e, 2)}>YES</button>
                </div>
            </div>
            <div className="form-group">
                <label className="control-label col-sm-3"></label>
                <div className="btn-toolbar col-sm-9" role="toolbar">
                    <div className="btn-group" role="group">
                        {(hasChanged)
                            ? <button type="submit" className="btn btn-default">Save</button>
                            : <button className="btn disabled">Save</button>
                        }
                        {(hasChanged && question.order < last) && <button className="btn" onClick={(e) => this.handleSaveAndNext(e, question)}>Save+Next</button>}
                        {(hasChanged && question.order == last) && <button className="btn" onClick={(e) => this.handleSaveAndUp(e, question)}>Save+Up</button>}
                        {(! hasChanged) && <button className="btn disabled">Save+Next</button>}

                    </div>
                    <div className="btn-group pull-right" role="group">
                        {(question.order > first)
                            ? <button className="btn" onClick={(e) => this.handlePrev(e, question)}>Prev</button>
                            : <button className="btn disabled">Prev</button>
                        }
                        <button className="btn" onClick={(e) => this.handleUp(e)}>Up</button>
                        {(question.order < last)
                            ? <button className="btn" onClick={(e) => this.handleNext(e, question)}>Next</button>
                            : <button className="btn disabled">Next</button>
                        }
                    </div>
                </div>
            </div>

        </form>;
    }
}