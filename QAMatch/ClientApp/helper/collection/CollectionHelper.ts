﻿export class CollectionHelper {

    public static toggleItem(numbers: number[], num: number): number[] {
        const result: number[] = (numbers.some(item => item == num))
            ? numbers.filter(item => item != num)
            : numbers.concat(num);
        return result;
    }

    public static addUnique(numbers: number[], num: number): number[] {
        const result: number[] = (! numbers.some(item => item == num))
            ? numbers.concat(num)
            : numbers;
        return result;
    }
}