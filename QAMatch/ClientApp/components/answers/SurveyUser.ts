﻿import { User } from "../../services/users/User";

export class SurveyUser {
    surveyId: number;
    user: User;
}