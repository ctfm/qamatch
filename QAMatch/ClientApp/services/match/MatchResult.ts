﻿import { MatchSource } from "./MatchSource";
import { AnswerTuple } from "./AnswerTuple";
import { DistanceValues } from "./DistanceValues";

export class MatchResult {
    sid: number;
    uid1: number;
    uid2: number;
    source1: MatchSource;
    source2: MatchSource;
    //scorePercent: number;
    deltaTups: AnswerTuple[];
    statistics: DistanceValues;

    public static ToString(result: MatchResult): string {
        if (result != null) {
            return "sid: " + result.sid
                + " | uid1: " + result.uid1
                + " | uid2: " + result.uid2
                + " | score: " + (result.statistics && result.statistics.scorePercent)
                + " | deltas: " + (result.deltaTups && result.deltaTups.length)
                ;
        }
        return null;
    }

    public static ToStrings(results: MatchResult[]): string {
        if (results != null && results.length > 0) {
            return results
                .map(result => MatchResult.ToString(result))
                .reduce((acc, item) => acc + "\n" + item);
        }
        return null;
    }
}