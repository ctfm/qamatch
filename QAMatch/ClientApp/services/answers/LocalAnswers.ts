﻿import { Answer } from './Answer';
import { IAnswerRepos } from './IAnswerRepos';

export class LocalAnswers implements IAnswerRepos {

    getUidsBySidAsync(sid: number): Promise<number[]> {
        //check
        const uids = LocalAnswers.AnswerList
            .map(answer => answer.userId);
        return new Promise<number[]>((resolve, reject) => resolve(uids));
    }

    public getAnswersBySidAsync(sid: number): Promise<Answer[]> {
        const answers = LocalAnswers.AnswerList
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswersByUSidAsync(uid: number, sid: number): Promise<Answer[]> {
        const answers = LocalAnswers.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswerById(id: number): Promise<Answer> {
        const answer = this.findAnswerById(id);
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise;
    }

    public getAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<Answer> {
        const answer: Answer = LocalAnswers.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid)
            .filter(answer => answer.questionId == qid)[0];
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise
    }

    public createAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (!this.findAnswerById(answer.id)) {
            const id = LocalAnswers.nextId();
            const newAnswer = { ...answer, id: id };
            LocalAnswers.AnswerList = LocalAnswers.AnswerList.concat(newAnswer);
            done = true;
        }
        const createPromise = new Promise<boolean>((resolve, reject) => {
            if (done) resolve(done)
            else reject("not done: answer: " + Answer.toString(answer))
        });
        return createPromise;
    }

    public updateAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (this.findAnswerById(answer.id)) {
            LocalAnswers.AnswerList = LocalAnswers.AnswerList
                .map(item => (item.userId == answer.userId && item.id == answer.id) ? answer : item);
            done = true;
        }
        const updatePromise = new Promise<boolean>((res, rej) => {
            if (done) res(done)
            else rej("not done: answer: " + Answer.toString(answer));
        });
        return updatePromise;
    }

    public deleteAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }


    private static AnswerList: Answer[];
    private static LastId: number = 0;

    constructor() {
        LocalAnswers.AnswerList = LocalAnswers.AnswerList || [];
        console.log("localanswers ctor AnswerList:\n" + LocalAnswers.toString(LocalAnswers.AnswerList));
        console.log("localanswers ctor LastId: " + LocalAnswers.LastId);
    }

    private static nextId() : number {
        LocalAnswers.LastId = LocalAnswers.LastId + 1;
        return LocalAnswers.LastId;
    }

    private findAnswerById(aid: number) {
        return LocalAnswers.AnswerList
            .filter(ans => ans.id == aid)[0] || null;
    }

    private static toString(answers: Answer[]) {
        const list: string = answers && answers.length > 0 && answers
            .map(answer => answer.id + " | " + answer.scale)
            .reduce((acc, a) => acc + "\n" + a);
        return list;
    }
}