﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import 'isomorphic-fetch';
import { DOMElement } from 'react';
import { SurveyUser } from './SurveyUser';
import { MatchHelper } from '../../helper/match/MatchHelper';
import { SidUidTuple } from '../../services/match/SidUidTuple';
import { IMatchRepos } from '../../services/match/IMatchRepos';
import { MatchResult } from '../../services/match/MatchResult';
import { ILogger } from '../../helper/logging/ILogger';
import { UrlHelper } from '../../helper/url/UrlHelper';
import { AnswerTuple } from '../../services/match/AnswerTuple';
import { User } from '../../services/users/User';
import { IUserRepos } from '../../services/users/IUserRepos';

interface MatchProps {
    routeProps: RouteComponentProps<{}>;
    logger: ILogger;
    userRepos: IUserRepos;
    matchRepos: IMatchRepos;
}

interface MatchState {
    loading: boolean;
    users: User[];
    matches: MatchResult[];
}

export class MatchResults extends React.Component<MatchProps, MatchState> {

    constructor(props: MatchProps) {
        super(props);
        this.state = {
            loading: true,
            users: [],
            matches: []
        };
    }

    public componentDidMount() {
        const sid: number = this.props.routeProps.match.params["sid"];
        this.fetchAndSetMatches(sid);
    }

    private fetchAndSetMatches(sid: number) {
        const logger = this.props.logger;
        this.props.matchRepos.getMatchesAsync(sid)
            .then(matches => Promise.all([
                this.props.userRepos.getUsersByIdsAsync(matches.map(m => m.uid2)),
                matches]))
            .then(data => {
                logger.debug("MatchResults.fetch users: " + (data[0] && data[0].length));
                logger.debug("MatchResults.fetch matches:\n" + MatchResult.ToStrings(data[1]));
                return data;
            })
            .then(data => this.setState((prev, props) => {
                return { loading: false, users: data[0], matches: data[1] };
            }))
            .catch(reason => logger.error("MatchResults.fetch reason: " + reason));
    }

    private getUserName(id: number) {
        const user: User = this.state.users && this.state.users
            .find(user => user.id == id);
        return user && user.name;
    }

    public render() {
        const sid = this.props.routeProps.match.params["sid"];
        let table = this.state.loading
            ? this.renderLoading()
            : this.renderTable(this.state.matches);
        return <div>
            <h1>Survey {sid} / Match Results</h1>
            {table}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Matches...</em></p >;
    }

    private renderTable(matches: MatchResult[]) {
        const sid: number = this.props.routeProps.match.params["sid"];
        const keyFunc = (match: MatchResult) => match.sid + "-" + match.uid1 + "-" + match.uid2;
        const sortedMatches = matches
            .slice()
            .sort((a, b) => b.statistics.scorePercent - a.statistics.scorePercent)
            .map(match => {
                return { ...match, scorePercent: Math.round(match.statistics.scorePercent) };
            });
        return <div><table className='table'>
            <thead>
                <tr>
                    <th>localAnswers</th>
                    <th>otherAnswers</th>
                    <th>score</th>
                </tr>
            </thead>
            <tbody>
                {sortedMatches.map(match =>
                    <tr key={keyFunc(match)}>

                        <td><Link to={UrlHelper.getLocalAnswersUrl(sid)}>my Answers</Link></td>
                        <td><Link to={UrlHelper.getAnswersetUrl(sid, match.uid2)}>{this.getUserName(match.uid2)}</Link></td>
                        <td><Link to={UrlHelper.getMatchResultsUrl2(sid, match.uid2)}>{match.scorePercent} %</Link></td>
                    </tr>
                )}
            </tbody>
        </table>
        </div>
        ;
    }
}