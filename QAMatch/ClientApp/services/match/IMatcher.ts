﻿import { Answer } from "../answers/Answer";
import { MatchResult } from "./MatchResult";
import { MatchSource } from "./MatchSource";

export interface IMatcher {
    getMatchAsync(source1: MatchSource, source2: MatchSource): Promise<MatchResult>;
    getMatchesAsync(source1: MatchSource, otherSources: MatchSource[]): Promise<MatchResult[]>;
}