﻿import { Answer } from './Answer';
import { IAnswerRepos } from './IAnswerRepos';

export class LocalAnswersInSession implements IAnswerRepos {

    getUidsBySidAsync(sid: number): Promise<number[]> {
        this.AnswerList = (this.hasAnswers()) ? this.AnswerList : this.getAnswersInSession();
        const uids = this.AnswerList
            .map(answer => answer.userId);
        return new Promise<number[]>((resolve, reject) => resolve(uids));
    }

    public getAnswersBySidAsync(sid: number): Promise<Answer[]> {
        this.AnswerList = this.getAnswerList();
        const answers = this.AnswerList
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswersByUSidAsync(uid: number, sid: number): Promise<Answer[]> {
        this.AnswerList = this.getAnswerList();
        const answers = this.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid);
        const promise = new Promise<Answer[]>((resolve, reject) => resolve(answers));
        return promise;
    }

    public getAnswerById(id: number): Promise<Answer> {
        const answer = this.findAnswerById(id);
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise;
    }

    public getAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<Answer> {
        this.AnswerList = this.getAnswerList();
        const answer: Answer = this.AnswerList
            .filter(answer => answer.userId == uid)
            .filter(answer => answer.surveyId == sid)
            .filter(answer => answer.questionId == qid)[0];
        const promise = new Promise<Answer>((res, rej) => res(answer));
        return promise
    }

    public createAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (!this.findAnswerById(answer.id)) {
            const id = this.nextId();
            const newAnswer = { ...answer, id: id };
            this.AnswerList = this.AnswerList.concat(newAnswer);
            this.setAnswersInSession(this.AnswerList);
            done = true;
        }
        const createPromise = new Promise<boolean>((resolve, reject) => {
            if (done) resolve(done)
            else reject("not done: answer: " + Answer.toString(answer))
        });
        return createPromise;
    }

    public updateAnswerAsync(answer: Answer): Promise<boolean> {
        //TODO
        let done: boolean = false;
        if (this.findAnswerById(answer.id)) {
            this.AnswerList = this.AnswerList
                .map(item => (item.userId == answer.userId && item.id == answer.id) ? answer : item);
            this.setAnswersInSession(this.AnswerList);
            done = true;
        }
        const updatePromise = new Promise<boolean>((res, rej) => {
            if (done) res(done)
            else rej("not done: answer: " + Answer.toString(answer));
        });
        return updatePromise;
    }

    public deleteAnswerByUSQIdAsync(uid: number, sid: number, qid: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }


    private AnswerList: Answer[];
    private LastId: number;
    private static AnswersKey: string = 'Answers';
    private static IdKey: string = 'LastId';

    constructor() {
        //this.AnswerList = [];   //check
        //this.setAnswersInSession(this.AnswerList); //check
        if (! this.getAnswersInSession()) {
            this.setAnswersInSession([]);
        }
        if (!this.getLastIdInSession()) {
            this.setLastIdInSession(0);
        }
        console.log("localanswers ctor AnswerList:\n" + LocalAnswersInSession.toString(this.AnswerList));
        console.log("localanswers ctor LastId: " + this.LastId);
    }

    private nextId(): number {
        this.LastId = (this.LastId > 0)
            ? this.LastId
            : this.getLastIdInSession();
        this.LastId = this.LastId + 1;
        this.setLastIdInSession(this.LastId);
        return this.LastId;
    }

    private getAnswerList() {
        const answerList: Answer[] = (this.hasAnswers())
            ? this.AnswerList
            : this.getAnswersInSession();
        return answerList;
    }

    private findAnswerById(aid: number) {
        this.AnswerList = this.getAnswerList();
        return this.AnswerList
            .filter(ans => ans.id == aid)[0] || null;
    }

    private hasAnswers() {
        const ok: boolean = this.AnswerList && this.AnswerList.length > 0;
        return ok;
    }

    private getAnswersInSession(): Answer[] {
        const val: string = sessionStorage.getItem(LocalAnswersInSession.AnswersKey);
        const answers: Answer[] = JSON.parse(val);
        return answers;
    }

    private setAnswersInSession(answers: Answer[]): boolean {
        const val: string = JSON.stringify(answers);
        sessionStorage.setItem(LocalAnswersInSession.AnswersKey, val);
        return true;
    }

    private getLastIdInSession(): number {
        const val: string = sessionStorage.getItem(LocalAnswersInSession.IdKey);
        const result: number = val && parseInt(val);
        return result;
    }

    private setLastIdInSession(id: number): boolean {
        const val: string = id && id.toString();
        sessionStorage.setItem(LocalAnswersInSession.IdKey, val);
        return true;
    }

    private static toString(answers: Answer[]) {
        const list: string = answers && answers.length > 0 && answers
            .map(answer => answer.id + " | " + answer.scale)
            .reduce((acc, a) => acc + "\n" + a);
        return list;
    }
}