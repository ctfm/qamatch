﻿export class AnswerTuple {
    qid: number;
    aid1: number;
    scale1: number;
    aid2: number;
    scale2: number;
    delta: number;
    weight: number;
    range: [number, number];
}