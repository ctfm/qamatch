﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';
import { Question } from '../../services/questions/Question';
import { IQuestionRepos } from '../../services/questions/IQuestionRepos';
import 'isomorphic-fetch';
import { UrlHelper } from '../../helper/url/UrlHelper';
import { ISurveyRepos } from '../../services/surveys/ISurveyRepos';
import { Survey } from '../../services/surveys/Survey';
import { SurveyUser } from '../match/SurveyUser';


interface SurveyProps {
    routeProps: RouteComponentProps<{}>;
    surveyRepos: ISurveyRepos;
    questionRepos: IQuestionRepos;
}

interface SurveyState {
    survey: Survey;
    questions: Question[];
    loading: boolean;
}

export class SurveyDetail extends React.Component<SurveyProps, SurveyState> {

    constructor(props: SurveyProps) {
        super(props);
        this.state = {
            survey: null,
            questions: [],
            loading: true
        };
    }

    public componentDidMount() {
        const sid: number = this.props.routeProps.match.params["sid"];
        this.fetchAndSetData(sid);
    }

    private fetchAndSetData(sid: number) {
        const questionsPromise = this.props.questionRepos.getQuestionsAsync(sid);
        const surveyPromise = this.props.surveyRepos.getSurveyByIdAsync(sid);
        Promise.all([surveyPromise, questionsPromise])
            .then(data => this.setState({
                survey: data[0],
                questions: data[1],
                loading: false
            }))
            .catch(reason => console.log("reason: " + reason));
    }

    public render() {
        const title = this.state.survey && this.state.survey.title;
        let content = this.state.loading
            ? this.renderLoading()
            : this.renderTable(this.state.survey, this.state.questions);
        return <div>
            <h1>{title}</h1>
            {content}
        </div>;
    }

    private renderLoading() {
        return <p><em>Loading Questions...</em></p>;
    }

    private renderTable(survey: Survey, questions: Question[]) {
        const sid = this.props.routeProps.match.params['sid'];
        const desc = survey && survey.description || "description ...";
        const sortedQuestions = questions
            .slice()
            .sort((q1, q2) => q1.order - q2.order);
        return <div>
            <div>
                <p>{desc}</p>
                <p>.</p>
            </div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th style={{ width: '1%' }}>#</th>
                            <th style={{ width: '1%' }}>#</th>
                            <th style={{ width: '50%' }}>Title</th>
                            <th>Choices</th>
                            <th>Scale</th>
                            <th>Weight</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sortedQuestions.map(question =>
                            <tr key={question.order}>
                                <td>{question.id}</td>
                                <td>{question.order}</td>
                                <td>
                                    <Link to={UrlHelper.getSurveyQuestionDetail(sid, question.id)}>{question.title}</Link>
                                </td>
                                <td>{question.choices && question.choices.length}</td>
                                <td>{question.scale}</td>
                                <td>{question.weight}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>;
    }
}