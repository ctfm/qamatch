﻿export class User {
    id: number;
    name: string;
    description: string;

    public static toString(user: User): string {
        if (user) {
            return "id: " + user.id
                + " | name: " + user.name
                ;
        }
        return null;
    }
}