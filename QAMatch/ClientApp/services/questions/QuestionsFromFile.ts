﻿import { IQuestionRepos } from "./IQuestionRepos";
import { Question } from "./Question";
import { QRange } from "./QRange";
import data1 from "./data/questions1.json";
import data2 from "./data/questions2.json";

export class QuestionsFromFile implements IQuestionRepos {

    public getQuestionsAsync(sid: number): Promise<Question[]> {
        const questions: Question[] = this.QuestionList
            .filter(q => q.surveyId == sid);
        const promise = new Promise<Question[]>((resolve, reject) => resolve(questions));
        return promise;
    }

    public getQuestionsRangeAsync(sid: number): Promise<QRange> {
        const questions: Question[] = this.QuestionList
            .filter(q => q.surveyId == sid);
        const length = questions && questions.length || 0;
        const range: QRange = {
            firstIndex: 1,
            lastIndex: length,
            total: length
        };
        const promise = new Promise<QRange>((resolve, reject) => {
            if (range != null) resolve(range)
            else reject("no data");
        });
        return promise;
    }

    public getQuestionByIdAsync(sid: number, id: number): Promise<Question> {
        const question: Question = this.findQuestionById(sid, id);
        const promise = new Promise<Question>((resolve, reject) => {
            if (question != null) resolve(question)
            else reject("no data");
        });
        return promise;
    }

    public getQuestionByIndexAsync(sid: number, idx: number): Promise<Question> {
        const question: Question = this.findQuestionByIndex(sid, idx);
        const promise = new Promise<Question>((resolve, reject) => {
            if (question) resolve(question)
            else reject("no data")
        });
        return promise;
    }



    public createQuestionAsync(sid: number, question: Question): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public updateQuestionAsync(sid: number, question: Question): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    public deleteQuestionAsync(sid: number, id: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    private QuestionList: Question[];

    constructor() {
        console.log("QuestionsFromFile data1: " + (data1 && data1.length));
        console.log("QuestionsFromFile data2: " + (data2 && data2.length));
        this.QuestionList = data1.concat(data2);
        console.log("QuestionsFromFile questions: " + this.QuestionList.length);
    }

    private findQuestionById(sid: number, id: number) {
        return this.QuestionList
            .filter(question => question.surveyId == sid)
            .filter(question => question.id == id)
        [0];
    }

    private findQuestionByIndex(sid: number, idx: number) {
        return this.QuestionList
            .filter(question => question.surveyId == sid)
            .filter(question => question.order == idx)
        [0];
    }
}