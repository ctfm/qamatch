﻿import { Question } from './Question';
import { QRange } from './QRange';

export interface IQuestionRepos {
    getQuestionsAsync(sid: number): Promise<Question[]>;
    getQuestionsRangeAsync(sid: number): Promise<QRange>;
    getQuestionByIdAsync(sid: number, id: number): Promise<Question>;
    getQuestionByIndexAsync(sid: number, idx: number): Promise<Question>;
    createQuestionAsync(sid: number, question: Question): Promise<boolean>;
    updateQuestionAsync(sid: number, question: Question): Promise<boolean>;
    deleteQuestionAsync(sid: number, id: number): Promise<boolean>;
}